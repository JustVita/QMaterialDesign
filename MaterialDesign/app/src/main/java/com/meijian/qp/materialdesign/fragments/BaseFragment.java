package com.meijian.qp.materialdesign.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.meijian.qp.materialdesign.MyApplication;

/**
 * 类名：com.meijian.qp.materialdesign.fragments
 * 时间：2018/7/14 16:43
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class BaseFragment extends Fragment {

    private Activity activity;

    public Context getContext(){
        if (activity == null){
            return MyApplication.getInstance();
        }
        return activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = getActivity();
    }
}
