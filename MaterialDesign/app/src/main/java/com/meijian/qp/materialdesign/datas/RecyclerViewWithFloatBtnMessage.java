package com.meijian.qp.materialdesign.datas;

import java.io.Serializable;

/**
 * 类名：com.meijian.qp.materialdesign.datas
 * 时间：2018/7/13 15:26
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class RecyclerViewWithFloatBtnMessage implements Serializable {

    private String title;

    public RecyclerViewWithFloatBtnMessage(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
