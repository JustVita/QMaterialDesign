package com.meijian.qp.materialdesign.activities;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.adapters.CustomAdapter;
import com.meijian.qp.materialdesign.datas.RecyclerViewWithFloatBtnMessage;
import com.meijian.qp.materialdesign.holder.CustomViewHolder;
import com.meijian.qp.materialdesign.utils.QSncakbarUtils;
import com.meijian.qp.materialdesign.utils.QToastUtil;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecyclerViewWithFloatBtnActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    @BindView(R.id.rv_iv_image)
    ImageView rvIvImage;
    @BindView(R.id.rv_toolbar)
    Toolbar rvToolbar;
    @BindView(R.id.rv_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.rv_fab)
    FloatingActionButton rvFab;
    private List<RecyclerViewWithFloatBtnMessage> mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);
        ButterKnife.bind(this);
        initData();
        initView();
    }

    private void initData() {

        mData = new ArrayList<>();

        for (int i = 0; i < 20; i++) {

            mData.add(new RecyclerViewWithFloatBtnMessage("this is a test " + i));
        }
    }

    private void initView() {

        initToolbar();
        initRecyclerView();
    }

    private void initRecyclerView() {

        CustomAdapter<RecyclerViewWithFloatBtnMessage> adapter = new CustomAdapter<RecyclerViewWithFloatBtnMessage>(this, mData, R.layout.recyclerview_floatbtn_item) {

            @Override
            public void convert(CustomViewHolder viewHolder, final RecyclerViewWithFloatBtnMessage recyclerViewWithFloatBtnMessage) {
//                TextView tv = viewHolder.getView(R.id.item);
//                tv.setText(recyclerViewWithFloatBtnMessage.getTitle());
                viewHolder.setText(R.id.item, recyclerViewWithFloatBtnMessage.getTitle());

                viewHolder.setOnClickListener(R.id.item, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        QSncakbarUtils.showBarShortTime(v, recyclerViewWithFloatBtnMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                QToastUtil.showToastShortTime(getApplicationContext(), "hello", QToastUtil.CONFIRM);
                            }
                        });
                    }
                });

                viewHolder.setOnLongClickListener(R.id.item, new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        QSncakbarUtils.showBarShortTime(v, recyclerViewWithFloatBtnMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                QToastUtil.showToastShortTime(getApplicationContext(), "hello", QToastUtil.CONFIRM);
                            }
                        });
                        return true;
                    }
                });
            }
        };
//        RecyclerViewWithFloatBarAdapter adapter = new RecyclerViewWithFloatBarAdapter(mData);
        //设置布局管理器
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
        //添加分割线为默认水平分割线
        rv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        //设置更新item动画为默认动画
        rv.setItemAnimator(new DefaultItemAnimator());

//        adapter.setOnItemClickListener(new RecyclerViewWithFloatBarAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                QSncakbarUtils.showBarShortTime(view, "click" + position, QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        QToastUtil.showToastShortTime(getApplicationContext(), "hello", QToastUtil.CONFIRM);
//                    }
//                });
//            }
//
//            @Override
//            public void onItemLongClick(View view, int position) {
//                QSncakbarUtils.showBarShortTime(view, "click" + position, QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        QToastUtil.showToastShortTime(getApplicationContext(), "hello", QToastUtil.CONFIRM);
//                    }
//                });
//            }
//        });
    }

    private void initToolbar() {
        //设置标题
        rvToolbar.setTitle("Recycle");
        //设置子标题
//        rvToolbar.setSubtitle("View");
        //设置logo
        rvToolbar.setLogo(R.drawable.ic_audiotrack_24dp);
        setSupportActionBar(rvToolbar);
        //设置返回图标
        rvToolbar.setNavigationIcon(R.drawable.ic_chevron_left_24dp);
        //设置返回图标点击监听
        rvToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //设置ct标题
        collapsingToolbar.setTitle("Title");
        //设置收缩后的标题颜色
        collapsingToolbar.setCollapsedTitleTextColor(Color.YELLOW);
        //设置放大时的标题颜色
        collapsingToolbar.setExpandedTitleTextColor(ColorStateList.valueOf(Color.RED));
        //设置ct图片背景
        rvIvImage.setImageResource(R.mipmap.birds);
        //设置toolbar菜单点击监听
        rvToolbar.setOnMenuItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drawer, menu);
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_a:
                break;
            case R.id.nav_b:
                break;
            case R.id.nav_c:
                break;
            case R.id.nav_d:
                break;
            case R.id.nav_e_a:
                break;
            case R.id.nav_e_b:
                break;
        }
        return true;
    }

    @OnClick(R.id.rv_fab)
    public void onViewClicked() {
        QSncakbarUtils.showBarShortTime(rvFab, "click", QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QToastUtil.showToastShortTime(getApplicationContext(), "hello", QToastUtil.CONFIRM);
            }
        });
    }
}
