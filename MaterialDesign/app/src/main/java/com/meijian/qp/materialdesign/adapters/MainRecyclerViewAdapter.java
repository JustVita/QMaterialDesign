package com.meijian.qp.materialdesign.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.datas.MainMessage;
import com.meijian.qp.materialdesign.utils.QScreenUtils;

import java.util.List;

/**
 * 类名：com.meijian.qp.materialdesign.adapters
 * 时间：2018/7/9 17:12
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.MainRecyclerViewHolder> {

    private List<MainMessage> mData;

    public MainRecyclerViewAdapter(List<MainMessage> mData) {
        this.mData = mData;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private MainRecyclerViewAdapter.OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(MainRecyclerViewAdapter.OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MainRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_cardview_item, parent, false);
        return new MainRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MainRecyclerViewHolder holder, int position) {

        holder.mTextView.setText(mData.get(position).getTitle() == null ? "" : mData.get(position).getTitle());

        int width = QScreenUtils.getScreenWidth(holder.mImageView.getContext());

        int height = (int) (Math.random() * 200 + 200);
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.mipmap.ic_launcher) //加载成功之前的占位图
                .error(R.mipmap.ic_launcher_round) //加载错误之后的错误图
//                .override(width / 2, height) //指定图片的尺寸,忽略imageView的大小，直接指定图片的宽高
//                .fitCenter() // 指定图片的缩放类型（等比例缩放，宽或者是高等于imageView的宽或者是高）
                .centerCrop() //指定图片的缩放类型（等比例缩放，直到图片的宽高都大于等于imageView的宽度，然后截取中间的显示）
//                .circleCrop() //指定图片的缩放类型（圆形）
                .skipMemoryCache(true) //跳过内存缓存
//                .diskCacheStrategy(DiskCacheStrategy.ALL) // 缓存所有版本的图像
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC); //智能选择缓存
//                .diskCacheStrategy(DiskCacheStrategy.DATA) //只缓存原来图片的分辨率
//                .diskCacheStrategy(DiskCacheStrategy.NONE) // 跳过磁盘缓存
//                .diskCacheStrategy(DiskCacheStrategy.RESOURCE); // 只缓存最终的图片
        Glide.with(holder.mImageView)
//                .asBitmap() //指定只允许加载静态图片
//                .asDrawable() // 只允许加载drawable
//                .asFile()     // 指定格式为file
//                .asGif()      // 只允许加载gif
                .load(mData.get(position).getUrl())
                .apply(requestOptions)
                .into(holder.mImageView);
//        holder.mImageView.setImageResource(mData.get(position).getImageId());

        //设置随机高度
        //TODO:优化加载图片
//        int width = QScreenUtils.getScreenWidth(holder.mImageView.getContext());
        ViewGroup.LayoutParams layoutParams = holder.mImageView.getLayoutParams();
        layoutParams.width = width / 2;
        layoutParams.height = height;
        holder.mImageView.setLayoutParams(layoutParams);

        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickListener.onItemClick(holder.itemView, pos);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickListener.onItemLongClick(holder.itemView, pos);
                    //拦截点击事件
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public class MainRecyclerViewHolder extends RecyclerView.ViewHolder {

        CardView mCardView;
        ImageView mImageView;
        TextView mTextView;

        public MainRecyclerViewHolder(View itemView) {
            super(itemView);
            mCardView = itemView.findViewById(R.id.cd_main);
            mImageView = itemView.findViewById(R.id.iv_main_item);
            mTextView = itemView.findViewById(R.id.tv_main_item);
        }
    }
}
