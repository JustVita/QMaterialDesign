package com.meijian.qp.materialdesign.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.datas.TabViewPagerMessage;

import java.util.ArrayList;

/**
 * 类名：com.meijian.qp.materialdesign.adapters
 * 时间：2018/7/5 17:57
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class TabViewPagerContentAdapter extends RecyclerView.Adapter<TabViewPagerContentAdapter.MyContentViewHolder> {

    private ArrayList<TabViewPagerMessage> mData; // 名字和图片

    public TabViewPagerContentAdapter(ArrayList<TabViewPagerMessage> data) {
        this.mData = data;
    }

    @NonNull
    @Override
    public MyContentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tabvp_content_item, parent, false);
        return new MyContentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyContentViewHolder holder, final int position) {

        holder.mTextView.setText(mData.get(position).getTitle());
        holder.mImageView.setImageResource(mData.get(position).getImageId());

        if (mOnGridItemClickListener != null) {

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnGridItemClickListener.onContentItemClick(holder.itemView, pos);
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnGridItemClickListener.onContentItemLongClick(holder.itemView, pos);
                    //拦截点击事件
                    return true;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private MyGridContentOnClickListener mOnGridItemClickListener;

    public void setOnGridContentItemClickListener(MyGridContentOnClickListener mOnGridItemClickListener) {
        this.mOnGridItemClickListener = mOnGridItemClickListener;

    }

    public interface MyGridContentOnClickListener {
        void onContentItemClick(View view, int position);

        void onContentItemLongClick(View view, int position);
    }

    public class MyContentViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView;
        private TextView mTextView;

        private MyContentViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.iv_content_icon);
            mTextView = itemView.findViewById(R.id.tv_content_text);
        }
    }
}
