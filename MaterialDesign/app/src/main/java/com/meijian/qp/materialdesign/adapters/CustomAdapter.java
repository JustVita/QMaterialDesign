package com.meijian.qp.materialdesign.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.meijian.qp.materialdesign.holder.CustomViewHolder;

import java.util.List;

/**
 * 类名：com.meijian.qp.materialdesign.adapters
 * 时间：2018/7/13 17:31
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public abstract class CustomAdapter<T> extends RecyclerView.Adapter<CustomViewHolder> {

    protected List<T> mData;
    protected int mLayoutId;
    protected Context mContext;
    protected LayoutInflater mInflater;

    public CustomAdapter(Context mContext, List<T> mData, int mLayoutId) {
        this.mData = mData;
        this.mLayoutId = mLayoutId;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomViewHolder holder = CustomViewHolder.get(mContext, parent, mLayoutId);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

        convert(holder, mData.get(position));
    }

    public abstract void convert(CustomViewHolder viewHolder, T t);

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
