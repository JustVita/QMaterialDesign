package com.meijian.qp.materialdesign.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.fragments.ContentFragment;
import com.meijian.qp.materialdesign.fragments.DetailFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabViewPagerActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.sliding_tabs)
    TabLayout slidingTabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    private MyPageAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_view_pager);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        initToolbar();
        initTabViewPager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drawer, menu);
        return true;
    }

    private void initToolbar() {
        toolbar.setTitle("Tab");
//        toolbar.setSubtitle("ViewPager");
//        toolbar.setLogo(R.drawable.ic_audiotrack_24dp);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.setOnMenuItemClickListener(this);

        collapsingToolbar.setTitle("Collapsing");
        ivImage.setImageResource(R.mipmap.bee);
    }

    private void initTabViewPager() {

        setUpViewPager(viewpager);

        slidingTabs.addTab(slidingTabs.newTab().setText("Grid"));
        slidingTabs.addTab(slidingTabs.newTab().setText("Content"));
        slidingTabs.setupWithViewPager(viewpager);
        slidingTabs.getTabAt(0).setText(myAdapter.getTitle(0));
        slidingTabs.getTabAt(1).setText(myAdapter.getTitle(1));
    }

    public void setUpViewPager(ViewPager viewpager){

        myAdapter = new MyPageAdapter(getSupportFragmentManager());
        myAdapter.addFragment(DetailFragment.newInstance("grid",0),"Grid");
        myAdapter.addFragment(ContentFragment.newInstance("content",1),"Content");
        viewpager.setAdapter(myAdapter);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_a:
                break;
            case R.id.nav_b:
                break;
            case R.id.nav_c:
                break;
            case R.id.nav_d:
                break;
            case R.id.nav_e_a:
                break;
            case R.id.nav_e_b:
                break;
        }

        return true;
    }

    class MyPageAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragments=new ArrayList<>();
        private List<String> mFragmentTitles =new ArrayList<>();

        public MyPageAdapter(FragmentManager fragmentManager){
            super(fragmentManager);//使用父类的构造函数
        }

        public String getTitle(int position){
            return  mFragmentTitles.get(position);
        }

        public void addFragment(Fragment fragment,String title){
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
}
