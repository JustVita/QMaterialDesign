package com.meijian.qp.materialdesign;

import android.app.Application;
import android.content.Context;

/**
 * 类名：com.meijian.qp.materialdesign
 * 时间：2018/7/14 16:45
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class MyApplication extends Application {

    private static MyApplication mInstance;

    /**
     * 获取context
     *
     * @return
     */
    public static Context getInstance(){
        if (mInstance == null){
            mInstance = new MyApplication();
        }
        return mInstance;
    }
}
