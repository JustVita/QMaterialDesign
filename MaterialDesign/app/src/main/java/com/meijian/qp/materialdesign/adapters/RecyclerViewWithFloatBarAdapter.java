package com.meijian.qp.materialdesign.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.datas.RecyclerViewWithFloatBtnMessage;

import java.util.List;

/**
 * 类名：com.meijian.qp.materialdesign.adapters
 * 时间：2018/7/6 17:13
 * 描述：RecyclerViewWithFloatBarAdapter
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class RecyclerViewWithFloatBarAdapter extends RecyclerView.Adapter<RecyclerViewWithFloatBarAdapter.MjViewHolder> {

    private List<RecyclerViewWithFloatBtnMessage> mData;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public RecyclerViewWithFloatBarAdapter(List<RecyclerViewWithFloatBtnMessage> data) {
        mData = data;
    }

    @NonNull
    @Override
    public RecyclerViewWithFloatBarAdapter.MjViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_floatbtn_item, parent, false);
        return new MjViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewWithFloatBarAdapter.MjViewHolder holder, int position) {

        holder.mTextView.setText(mData.get(position).getTitle());

        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickListener.onItemClick(holder.itemView, pos);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickListener.onItemLongClick(holder.itemView, pos);
                    //拦截点击事件
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MjViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextView;

        private MjViewHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.item);
        }
    }
}
