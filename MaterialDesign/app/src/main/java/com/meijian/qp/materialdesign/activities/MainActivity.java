package com.meijian.qp.materialdesign.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.adapters.CustomAdapter;
import com.meijian.qp.materialdesign.datas.MainMessage;
import com.meijian.qp.materialdesign.holder.CustomViewHolder;
import com.meijian.qp.materialdesign.utils.QSncakbarUtils;
import com.meijian.qp.materialdesign.utils.QToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    public static final String AGNES = "http://t.bebove.com/data/members/agnes2018071192313.jpg";
    public static final String GIF = "http://t.bebove.com/data/members/cartoon120180711103049.gif";
    public static final String RABBIT = "http://t.bebove.com/data/members/rabbit20180711133122.jpg";
    public static final String BEE = "http://t.bebove.com/data/members/bee20180711132456.jpg";
    public static final String BIRDS = "http://t.bebove.com/data/members/birds20180711132513.jpg";
    public static final String MINION = "http://t.bebove.com/data/members/minion20180711132544.jpg";
    public static final String PEA = "http://t.bebove.com/data/members/pea20180711133103.jpg";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rl_main)
    RecyclerView rlMain;
    private List<MainMessage> mData;


    private CustomAdapter<MainMessage> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initData();
        initView();
    }

    private void initData() {

        mData = new ArrayList<>();

        //使用url加载图片
        //
//        mData.add(new MainMessage("TabViewPager", AGNES));
//        mData.add(new MainMessage("RVWithFloatBtn", RABBIT));
//        mData.add(new MainMessage("MaterialDesign", BEE));
//        mData.add(new MainMessage("MaterialDesign", BIRDS));
//        mData.add(new MainMessage("MaterialDesign", MINION));
//        mData.add(new MainMessage("MaterialDesign", PEA));
//        mData.add(new MainMessage("MaterialDesign", GIF));
//
//        mData.add(new MainMessage("MaterialDesign", AGNES));
//        mData.add(new MainMessage("MaterialDesign", RABBIT));
//        mData.add(new MainMessage("MaterialDesign", BEE));
//        mData.add(new MainMessage("MaterialDesign", BIRDS));
//        mData.add(new MainMessage("MaterialDesign", MINION));
//        mData.add(new MainMessage("MaterialDesign", PEA));
//        mData.add(new MainMessage("MaterialDesign", GIF));

        mData.add(new MainMessage("TabViewPager", R.mipmap.agnes));
        mData.add(new MainMessage("RVWithFloatBtn", R.mipmap.bee));
        mData.add(new MainMessage("birds", R.mipmap.birds));
        mData.add(new MainMessage("pea", R.mipmap.pea));
        mData.add(new MainMessage("minion", R.mipmap.minion));
        mData.add(new MainMessage("rabbit", R.mipmap.rabbit));
        mData.add(new MainMessage("agnes", R.mipmap.agnes));
        mData.add(new MainMessage("bee", R.mipmap.bee));
        mData.add(new MainMessage("birds", R.mipmap.birds));
        mData.add(new MainMessage("pea", R.mipmap.pea));
        mData.add(new MainMessage("minion", R.mipmap.minion));
        mData.add(new MainMessage("rabbit", R.mipmap.rabbit));
        mData.add(new MainMessage("agnes", R.mipmap.agnes));
        mData.add(new MainMessage("bee", R.mipmap.bee));
        mData.add(new MainMessage("birds", R.mipmap.birds));
        mData.add(new MainMessage("pea", R.mipmap.pea));
        mData.add(new MainMessage("minion", R.mipmap.minion));
        mData.add(new MainMessage("rabbit", R.mipmap.rabbit));
    }

    private void initView() {

        toolbar.setTitle("MaterialDesign");
//        toolbar.setSubtitle("Design");
//        toolbar.setLogo(R.drawable.ic_audiotrack_24dp);

        setSupportActionBar(toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_adb_24dp);
        toolbar.setOnMenuItemClickListener(this);

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        //此方法只解决item 不跳动的问题，不能解决空白等问题
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        rlMain.setLayoutManager(staggeredGridLayoutManager);
        mAdapter = new CustomAdapter<MainMessage>(this,mData,R.layout.main_cardview_item){

            @Override
            public void convert(CustomViewHolder viewHolder, final MainMessage message) {
                viewHolder.setText(R.id.tv_main_item,message.getTitle());
                viewHolder.setImageResource(R.id.iv_main_item,message.getImageId());

                viewHolder.setOnClickListener(R.id.cd_main, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (message.getTitle().equals("TabViewPager")){
                            ActionStart(MainActivity.this, TabViewPagerActivity.class);
                        }else if (message.getTitle().equals("RVWithFloatBtn")){
                            ActionStart(MainActivity.this, RecyclerViewWithFloatBtnActivity.class);
                        }else {
                            QSncakbarUtils.showBarShortTime(v, message.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    QToastUtil.showToastShortTime(getApplicationContext(), "hello", QToastUtil.CONFIRM);
                                }
                            });
                        }
                    }
                });

                viewHolder.setOnLongClickListener(R.id.cd_main, new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (message.getTitle().equals("TabViewPager")){
                            ActionStart(MainActivity.this, TabViewPagerActivity.class);
                        }else if (message.getTitle().equals("RVWithFloatBtn")){
                            ActionStart(MainActivity.this, RecyclerViewWithFloatBtnActivity.class);
                        }else {
                            QSncakbarUtils.showBarShortTime(v, message.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    QToastUtil.showToastShortTime(getApplicationContext(), "hello", QToastUtil.CONFIRM);
                                }
                            });
                        }
                        return true;
                    }
                });
            }
        };
        rlMain.setAdapter(mAdapter);
        rlMain.setItemAnimator(new DefaultItemAnimator());
//        mAdapter = new MainRecyclerViewAdapter(mData);
//        rlMain.setAdapter(mAdapter);
//        rlMain.setItemAnimator(new DefaultItemAnimator());
//
//        mAdapter.setOnItemClickListener(new MainRecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                switch (position) {
//                    case 0:
//                        ActionStart(MainActivity.this, TabViewPagerActivity.class);
//                        break;
//                    case 1:
//                        ActionStart(MainActivity.this, RecyclerViewWithFloatBtnActivity.class);
//                        break;
//                }
//            }
//
//            @Override
//            public void onItemLongClick(View view, int position) {
//
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drawer, menu);
        return true;
    }

    private void ActionStart(Activity activity, Class<?> cls) {
        Intent intent = new Intent(activity, cls);
        startActivity(intent);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_a:
                add(2, new MainMessage("insert", R.mipmap.pea));
                break;
            case R.id.nav_b:
                remove(2);
                break;
            case R.id.nav_c:
                break;
            case R.id.nav_d:
                break;
            case R.id.nav_e_a:
                break;
            case R.id.nav_e_b:
                break;
        }
        return true;
    }

    public void add(int position, MainMessage message) {
        if (mData.size() - position < 0) return;
        mData.add(message);
        mAdapter.notifyItemInserted(position);
        mAdapter.notifyItemRangeChanged(position, mData.size() - position);
    }

    public void remove(int position) {
        if (mData.size() - position < 1) return;
        mData.remove(position);
        mAdapter.notifyItemRemoved(position);
        mAdapter.notifyItemRangeChanged(position, mData.size() - position);
    }
}
