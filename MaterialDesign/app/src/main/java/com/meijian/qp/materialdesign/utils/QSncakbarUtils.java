package com.meijian.qp.materialdesign.utils;

import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.IntDef;
import android.support.annotation.IntRange;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meijian.qp.materialdesign.R;

/**
 * 类名：com.meijian.qp.materialdesign.utils
 * 时间：2018/7/6 11:22
 * 描述：Snackbar的简单使用
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class QSncakbarUtils {

    /**
     * 信息类型
     */
    public static final int INFO = 1;
    /**
     * 确认信息类型
     */
    public static final int CONFIRM = 2;
    /**
     * 警告类型
     */
    public static final int WARNING = 3;
    /**
     * 错误类型
     */
    public static final int ALERT = 4;

    /**
     * 信息类型的背景颜色
     */
    public final static int BLUE = 0xff2195f3;
    /**
     * 确认信息类型背景颜色
     */
    public final static int GREEN = 0xff4caf50;
    /**
     * 警告类型背景颜色
     */
    public final static int ORANGE = 0xffffc107;
    /**
     * 错误类型背景颜色
     */
    public final static int RED = 0xfff44336;
    /**
     * action文本颜色  白色
     */
    public final static int WHITE = 0xffFFFFFF;

    /**
     * 消息类型   替代Java中的枚举类型
     */
    @IntDef({INFO, CONFIRM, WARNING, ALERT})
    private @interface MessageType {

    }

    /**
     * 显示Snackbar，时长（1570ms）,自定义颜色
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param messageColor  消息文本颜色
     * @param backgroundColor   背景颜色
     */
    public static void showBarShortTime(@NonNull View view, @NonNull String message,
                                        @ColorInt int messageColor, @ColorInt int backgroundColor) {
        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        setSnackbarColor(sb, messageColor, backgroundColor);
        sb.show();
    }

    /**
     * 显示Snackbar， 时长（1570ms） 自定义颜色
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param messageColor  消息文本颜色
     * @param backgroundColor   背景颜色
     */
    public static void showBarLongTime(@NonNull View view, @NonNull String message,
                                       @ColorInt int messageColor, @ColorInt int backgroundColor) {
        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        setSnackbarColor(sb, messageColor, backgroundColor);
        sb.show();
    }

    /**
     * 显示Snackbar，可自定义时长及颜色
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param duration  显示时长 ms
     * @param messageColor  消息文本颜色
     * @param backgroundColor   背景颜色
     */
    public static void showCustomCATSnackbar(@NonNull View view, @NonNull String message,
                                             @IntRange(from = 1) int duration,
                                             @ColorInt int messageColor, @ColorInt int backgroundColor) {
        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE).setDuration(duration);
        setSnackbarColor(sb, messageColor, backgroundColor);
        sb.show();
    }


    /**
     * 显示Snackbar，时长（1570ms），可选预设类型
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param type      可选类型
     */
    public static void showBarShortTime(@NonNull View view, @NonNull String message,
                                        @MessageType int type) {

        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        setSnackbarMessageType(sb, type);
        sb.show();
    }

    /**
     * 显示Snackbar，时长（1570ms），可选预设类型
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param type      需要显示的消息类型
     * @param text       action显示文本
     * @param listener   点击监听
     */
    public static void showBarShortTime(@NonNull View view, @NonNull String message,
                                        @MessageType int type, @NonNull CharSequence text,
                                        @NonNull View.OnClickListener listener) {

        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_SHORT).setAction(text, listener).setActionTextColor(WHITE);
        setSnackbarMessageType(sb, type);
        sb.show();
    }

    /**
     * 显示Snackbar，时长（2750ms），可选预设类型
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param type      需要显示的消息类型
     */
    public static void showBarLongTime(@NonNull View view, @NonNull String message,
                                       @MessageType int type) {

        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        setSnackbarMessageType(sb, type);
        sb.show();
    }

    /**
     * 显示Snackbar，可选预设类型，长时间（2750ms）
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param type      需要显示的类型
     * @param text      action文本
     * @param listener  点击监听
     */
    public static void showBarLongTime(@NonNull View view, @NonNull String message,
                                       @MessageType int type, @NonNull CharSequence text,
                                       @NonNull View.OnClickListener listener) {

        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction(text, listener).setActionTextColor(WHITE);
        setSnackbarMessageType(sb, type);
        sb.show();
    }

    /**
     * 自定义时长 可选预设类型
     *
     * @param view  parent view
     * @param message   需要显示的消息
     * @param duration  需要显示的时长 ms
     * @param type      需要显示的消息类型
     */
    public static void showCustomTimeSnackbar(@NonNull View view, @NonNull String message,
                                              @IntRange(from = 1) int duration, @MessageType int type) {
        Snackbar sb = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE).setDuration(duration);
        setSnackbarMessageType(sb, type);
        sb.show();
    }

    /**
     * 设置Snackbar背景颜色
     *
     * @param snackbar  Snackbar
     * @param backgroundColor   背景颜色
     */
    private static void setSnackbarBgColor(Snackbar snackbar, @ColorInt int backgroundColor){

        View view = snackbar.getView();
        view.setBackgroundColor(backgroundColor);
    }


    /**
     * 设置Snackbar文字和背景颜色
     *
     * @param snackbar  Snackbar
     * @param messageColor  文字颜色
     * @param backgroundColor 背景颜色
     */
    private static void setSnackbarColor(Snackbar snackbar, @ColorInt int messageColor,
                                        @ColorInt int backgroundColor) {
        View view = snackbar.getView();
        view.setBackgroundColor(backgroundColor);
        TextView textView = view.findViewById(R.id.snackbar_text);
        textView.setTextColor(messageColor);
    }


    /**
     * 设置预设消息类型
     *
     * @param sb    Snackbar
     * @param type  消息类型，INFO，CONFIRM, WARRING , ALERT
     */
    private static void setSnackbarMessageType(Snackbar sb, @MessageType int type) {

        switch (type){
            case INFO:
                setSnackbarBgColor(sb,BLUE);
                break;
            case CONFIRM:
                setSnackbarBgColor(sb,GREEN);
                break;
            case WARNING:
                setSnackbarBgColor(sb,ORANGE);
                break;
            case ALERT:
                setSnackbarColor(sb, Color.YELLOW,RED);
                break;

        }
    }

    /**
     * 向Snackbar中添加view
     *
     * @param snackbar Snackbar
     * @param layoutId 需要添加的布局id
     * @param index     新加布局在Snackbar中的位置
     */
    public static void snackbarAddView(Snackbar snackbar, @LayoutRes int layoutId, int index){

        View view = snackbar.getView();
        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) view;

        View addView = LayoutInflater.from(snackbar.getContext()).inflate(layoutId, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.gravity = Gravity.CENTER_VERTICAL;
        snackbarLayout.addView(addView,index,layoutParams);
    }


}
