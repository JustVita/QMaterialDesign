package com.meijian.qp.materialdesign.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.meijian.qp.materialdesign.holder.CustomViewHolder;
import com.meijian.qp.materialdesign.holder.MultiItemTypeSupport;

import java.util.List;

/**
 * 类名：com.meijian.qp.materialdesign.adapters
 * 时间：2018/7/14 9:52
 * 描述：此类用于RecyclerView多类型item的通用类
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public abstract class MultiItemCustomAdapter<T> extends CustomAdapter<T> {

    protected MultiItemTypeSupport<T> mMultiItemTypeSupport;

    public MultiItemCustomAdapter(Context mContext, List<T> mData, MultiItemTypeSupport<T> multiItemTypeSupport) {
        super(mContext, mData, -1);
        mMultiItemTypeSupport = multiItemTypeSupport;
    }

    @Override
    public int getItemViewType(int position) {
        //将当前item对象中的position和数据中的需要视图type传入，让用户进行判断，返回相应的type
        return mMultiItemTypeSupport.getItemViewType(position, mData.get(position));
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //将当前item的viewType传入，让用户进行判断，根据相应的viewType返回相应的layoutId,用于创建相应的viewHolder
        int layoutId = mMultiItemTypeSupport.getLayoutId(viewType);
        CustomViewHolder customViewHolder = CustomViewHolder.get(mContext, parent, layoutId);
        return customViewHolder;
    }
}
