package com.meijian.qp.materialdesign.holder;

/**
 * 类名：com.meijian.qp.materialdesign.holder
 * 时间：2018/7/14 9:50
 * 描述：此接口用于支持多类型item
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public interface MultiItemTypeSupport<T> {
    int getLayoutId(int itemType);

    int getItemViewType(int position, T t);
}
