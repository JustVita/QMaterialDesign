package com.meijian.qp.materialdesign.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.adapters.CustomAdapter;
import com.meijian.qp.materialdesign.datas.TabViewPagerMessage;
import com.meijian.qp.materialdesign.divide.CustomGridDividerDecoration;
import com.meijian.qp.materialdesign.holder.CustomViewHolder;
import com.meijian.qp.materialdesign.utils.QSncakbarUtils;
import com.meijian.qp.materialdesign.utils.QToastUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TITLE = "title";
    private static final String ID = "id";
    @BindView(R.id.grid_rv_recycler)
    RecyclerView gridRvRecycler;
    Unbinder unbinder;
    private ArrayList<TabViewPagerMessage> mData;

    // TODO: Rename and change types of parameters
    private String mTitile;
    private int mId;

    public DetailFragment() {
        // Required empty public constructor
    }

    /**
     * 单例
     * @param title
     * @param id
     * @return
     */
    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(String title, int id) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putInt(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitile = getArguments().getString(TITLE);
            mId = getArguments().getInt(ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tabvp_fragment_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initData();

//        MultiItemTypeSupport<TabViewPagerMessage> support = new MultiItemTypeSupport<TabViewPagerMessage>() {
//            @Override
//            public int getLayoutId(int itemType) {
//
//                int layoutId;
//                if (itemType == 0) {
//                    //TODO:根据当前item需要的itemType返回响应的layoutId
//                    //TODO:返回的layoutId用于创建相应的viewHolder
//                    layoutId = 0;
//                } else {
//                    //TODO:返回默认值
//                    layoutId = 1;
//                }
//                return R.layout.tabvp_grid_item;
//            }
//
//            @Override
//            public int getItemViewType(int position, TabViewPagerMessage tabViewPagerMessage) {
//                int viewType = 0;
//                //返回的T,进行数据判断，返回相应的viewType
//                if (mData.size() == 0) {
//
//                    //TODO:集合为空，返回空View标识
//                } else if (mData.get(position) == null) {
//
//                    //TODO:item对象为空，返回进度条标识
//                } else if (tabViewPagerMessage.equals("类型")) {
//
//                    //TODO:根据需要类型返回相应View标识
//                } else {
//
//                    //TODO：返回默认值
//                }
//                return viewType;
//            }
//        };
//        MultiItemCustomAdapter<TabViewPagerMessage> adapter = new MultiItemCustomAdapter<TabViewPagerMessage>(getActivity(), mData, support) {
//
//            @Override
//            public void convert(CustomViewHolder viewHolder, final TabViewPagerMessage tabViewPagerMessage) {
//                viewHolder.setText(R.id.grid_text, tabViewPagerMessage.getTitle());
//                viewHolder.setImageResource(R.id.grid_image, tabViewPagerMessage.getImageId());
//
//                viewHolder.setOnClickListener(R.id.ll_tvp_item, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        QSncakbarUtils.showBarShortTime(v, tabViewPagerMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                QToastUtil.showToastShortTime(v.getContext(), "hello", QToastUtil.CONFIRM);
//                            }
//                        });
//                    }
//                });
//
//                viewHolder.setOnLongClickListener(R.id.ll_tvp_item, new View.OnLongClickListener() {
//                    @Override
//                    public boolean onLongClick(View v) {
//                        QSncakbarUtils.showBarShortTime(v, tabViewPagerMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                QToastUtil.showToastShortTime(v.getContext(), "hello", QToastUtil.CONFIRM);
//                            }
//                        });
//                        return true;
//                    }
//                });
//            }
//        };

        CustomAdapter<TabViewPagerMessage> adapter = new CustomAdapter<TabViewPagerMessage>(getContext(),mData,R.layout.tabvp_grid_item) {
            @Override
            public void convert(CustomViewHolder viewHolder, final TabViewPagerMessage tabViewPagerMessage) {
                viewHolder.setText(R.id.grid_text,tabViewPagerMessage.getTitle());
                viewHolder.setImageResource(R.id.grid_image,tabViewPagerMessage.getImageId());

                viewHolder.setOnClickListener(R.id.ll_tvp_grid_item, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        QSncakbarUtils.showBarShortTime(v, tabViewPagerMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                QToastUtil.showToastShortTime(v.getContext(), "hello", QToastUtil.CONFIRM);
                            }
                        });
                    }
                });

                viewHolder.setOnLongClickListener(R.id.ll_tvp_grid_item, new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        QSncakbarUtils.showBarShortTime(v, tabViewPagerMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                QToastUtil.showToastShortTime(v.getContext(), "hello", QToastUtil.CONFIRM);
                            }
                        });
                        return true;
                    }
                });
            }
        };

        gridRvRecycler.setAdapter(adapter);
        gridRvRecycler.setLayoutManager(new GridLayoutManager(getContext(), 2));
        gridRvRecycler.addItemDecoration(new CustomGridDividerDecoration(view.getContext()));
        gridRvRecycler.setItemAnimator(new DefaultItemAnimator());

//        TabViewPagerGridAdapter gridAdapter = new TabViewPagerGridAdapter(mData);
//        gridRvRecycler.setAdapter(gridAdapter);
//        gridRvRecycler.setLayoutManager(new GridLayoutManager(getContext(), 2));
//        gridRvRecycler.addItemDecoration(new CustomGridDividerDecoration(view.getContext()));
//        gridAdapter.setOnGridItemClickListener(new TabViewPagerGridAdapter.OnGridItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                QSncakbarUtils.showBarShortTime(view, "点击的是：" + position,
//                        QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                QToastUtil.showToastLongTime(getContext(), "confirm", QToastUtil.CONFIRM);
//                            }
//                        });
//            }
//
//            @Override
//            public void onItemLongClick(View view, int position) {
//                QSncakbarUtils.showBarShortTime(view, "点击的是：" + position,
//                        QSncakbarUtils.CONFIRM, "确定", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                QToastUtil.showToastLongTime(getContext(), "ERROR", QToastUtil.ERROR);
//                            }
//                        });
//            }
//        });
    }

    private void initData() {
        mData = new ArrayList<>();

        mData.add(new TabViewPagerMessage("RABBIT", R.mipmap.rabbit));
        mData.add(new TabViewPagerMessage("PEA", R.mipmap.pea));
        mData.add(new TabViewPagerMessage("MINION", R.mipmap.minion));
        mData.add(new TabViewPagerMessage("BIRDS", R.mipmap.birds));
        mData.add(new TabViewPagerMessage("AGNES", R.mipmap.agnes));
        mData.add(new TabViewPagerMessage("BEE", R.mipmap.bee));

        mData.add(new TabViewPagerMessage("RABBIT", R.mipmap.rabbit));
        mData.add(new TabViewPagerMessage("PEA", R.mipmap.pea));
        mData.add(new TabViewPagerMessage("MINION", R.mipmap.minion));
        mData.add(new TabViewPagerMessage("BIRDS", R.mipmap.birds));
        mData.add(new TabViewPagerMessage("AGNES", R.mipmap.agnes));
        mData.add(new TabViewPagerMessage("BEE", R.mipmap.bee));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
