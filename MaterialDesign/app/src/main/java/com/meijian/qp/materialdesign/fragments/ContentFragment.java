package com.meijian.qp.materialdesign.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.adapters.CustomAdapter;
import com.meijian.qp.materialdesign.datas.TabViewPagerMessage;
import com.meijian.qp.materialdesign.holder.CustomViewHolder;
import com.meijian.qp.materialdesign.utils.QSncakbarUtils;
import com.meijian.qp.materialdesign.utils.QToastUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContentFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TITLE = "title";
    private static final String ID = "id";
    private ArrayList<TabViewPagerMessage> mData;
    @BindView(R.id.recycler_content)
    RecyclerView recyclerContent;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String mTitle;
    private int mId;

    public ContentFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ContentFragment newInstance(String title, int id) {
        ContentFragment fragment = new ContentFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putInt(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString(TITLE);
            mId = getArguments().getInt(ID);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tabvp_fragment_content, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        initData();

        CustomAdapter<TabViewPagerMessage> adapter = new CustomAdapter<TabViewPagerMessage>(getActivity(),mData,R.layout.tabvp_content_item) {
            @Override
            public void convert(CustomViewHolder viewHolder, final TabViewPagerMessage tabViewPagerMessage) {
                viewHolder.setText(R.id.tv_content_text,tabViewPagerMessage.getTitle());
                viewHolder.setImageResource(R.id.iv_content_icon,tabViewPagerMessage.getImageId());

                viewHolder.setOnClickListener(R.id.ll_tvp_content_item, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        QSncakbarUtils.showBarShortTime(v, tabViewPagerMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                QToastUtil.showToastShortTime(v.getContext(), "hello", QToastUtil.CONFIRM);
                            }
                        });
                    }
                });

                viewHolder.setOnLongClickListener(R.id.ll_tvp_content_item, new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        QSncakbarUtils.showBarShortTime(v, tabViewPagerMessage.getTitle(), QSncakbarUtils.INFO, "确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                QToastUtil.showToastShortTime(v.getContext(), "hello", QToastUtil.CONFIRM);
                            }
                        });
                        return true;
                    }
                });
            }
        };

        recyclerContent.setAdapter(adapter);
        recyclerContent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerContent.addItemDecoration(new DividerItemDecoration(view.getContext(),DividerItemDecoration.VERTICAL));
        recyclerContent.setItemAnimator(new DefaultItemAnimator());


//        TabViewPagerContentAdapter tabViewPagerContentAdapter = new TabViewPagerContentAdapter(mData);
//
//        recyclerContent.setAdapter(tabViewPagerContentAdapter);
//
//        recyclerContent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        recyclerContent.addItemDecoration(new DividerItemDecoration(view.getContext(),DividerItemDecoration.VERTICAL));
//        tabViewPagerContentAdapter.setOnGridContentItemClickListener(new TabViewPagerContentAdapter.MyGridContentOnClickListener() {
//            @Override
//            public void onContentItemClick(View view, int position) {
//                QSncakbarUtils.showBarShortTime(view, "点击的是：" + position,
//                        QSncakbarUtils.WARNING, "确定", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                QToastUtil.showToastShortTime(getContext(), "ERROR", QToastUtil.ERROR);
//                            }
//                        });
//            }
//
//            @Override
//            public void onContentItemLongClick(View view, int position) {
//                QSncakbarUtils.showBarShortTime(view, "点击的是：" + position,
//                        QSncakbarUtils.WARNING, "确定", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                QToastUtil.showToastShortTime(getContext(), "ERROR", QToastUtil.ERROR);
//                            }
//                        });
//            }
//        });
    }

    private void initData() {

        mData = new ArrayList<>();
        mData.add(new TabViewPagerMessage("RABBIT", R.mipmap.rabbit));
        mData.add(new TabViewPagerMessage("PEA", R.mipmap.pea));
        mData.add(new TabViewPagerMessage("MINION", R.mipmap.minion));
        mData.add(new TabViewPagerMessage("BIRDS", R.mipmap.birds));
        mData.add(new TabViewPagerMessage("AGNES", R.mipmap.agnes));
        mData.add(new TabViewPagerMessage("BEE", R.mipmap.bee));

        mData.add(new TabViewPagerMessage("RABBIT", R.mipmap.rabbit));
        mData.add(new TabViewPagerMessage("PEA", R.mipmap.pea));
        mData.add(new TabViewPagerMessage("MINION", R.mipmap.minion));
        mData.add(new TabViewPagerMessage("BIRDS", R.mipmap.birds));
        mData.add(new TabViewPagerMessage("AGNES", R.mipmap.agnes));
        mData.add(new TabViewPagerMessage("BEE", R.mipmap.bee));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
