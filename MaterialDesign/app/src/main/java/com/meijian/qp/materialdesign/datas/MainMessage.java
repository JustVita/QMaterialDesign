package com.meijian.qp.materialdesign.datas;

import java.io.Serializable;

/**
 * 类名：com.meijian.qp.materialdesign.datas
 * 时间：2018/7/9 17:25
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class MainMessage implements Serializable{

    private String title;
    private int imageId;
    private String url;

    public MainMessage(String title, int imageId) {
        this.title = title;
        this.imageId = imageId;
    }

    public MainMessage(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
