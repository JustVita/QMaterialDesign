package com.meijian.qp.materialdesign.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.IntDef;
import android.support.annotation.IntRange;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 类名：com.meijian.qp.materialdesign.utils
 * 时间：2018/7/6 14:48
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class QToastUtil {

    /**
     * 信息类型
     */
    public static final int INFO = 1;
    /**
     * 确认信息类型
     */
    public static final int CONFIRM = 2;
    /**
     * 警告类型
     */
    public static final int WARNING = 3;
    /**
     * 错误类型
     */
    public static final int ERROR = 4;

    /**
     * 信息类型的背景颜色
     */
    public final static int BLUE = 0xff2195f3;
    /**
     * 确认信息类型背景颜色
     */
    public final static int GREEN = 0xff4caf50;
    /**
     * 警告类型背景颜色
     */
    public final static int ORANGE = 0xffffc107;
    /**
     * 错误类型背景颜色
     */
    public final static int RED = 0xfff44336;
    /**
     * action文本颜色  白色
     */
    public final static int WHITE = 0xffFFFFFF;

    /**
     * 消息类型   替代Java中的枚举类型
     */
    @IntDef({INFO, CONFIRM, WARNING, ERROR})
    private @interface MessageType {

    }

    /**
     * 显示toast短时长
     *
     * @param context         上下文
     * @param message         显示信息
     * @param messageColor    文本颜色
     * @param backgroundColor 背景颜色
     */
    @SuppressLint("ShowToast")
    public static void showToastShortTime(Context context, @NonNull String message,
                                          @ColorInt int messageColor, @ColorInt int backgroundColor) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        setToastColor(toast, messageColor, backgroundColor);
        toast.show();
    }

    /**
     * 显示toast 长时长
     *
     * @param context         上下文
     * @param message         显示信息
     * @param messageColor    文本颜色
     * @param backgroundColor 背景颜色
     */
    @SuppressLint("ShowToast")
    public static void showToastLongTime(Context context, @NonNull String message,
                                         @ColorInt int messageColor, @ColorInt int backgroundColor) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        setToastColor(toast, messageColor, backgroundColor);
        toast.show();
    }

    /**
     * 显示toast，自定义时长
     *
     * @param context         上下文
     * @param message         显示信息
     * @param duration        显示时长
     * @param messageColor    文本颜色
     * @param backgroundColor 背景颜色
     */
    @SuppressLint("ShowToast")
    public static void showToastCustomTime(Context context, @NonNull String message, @IntRange(from = 1) int duration,
                                           @ColorInt int messageColor, @ColorInt int backgroundColor) {
        Toast toast = Toast.makeText(context, message, duration);
        setToastColor(toast, messageColor, backgroundColor);
        toast.show();
    }

    /**
     * 显示toast，短时长，可选类型
     *
     * @param context 上下文
     * @param message 显示信息
     * @param type    显示类型
     */
    @SuppressLint("ShowToast")
    public static void showToastShortTime(Context context, @NonNull String message, @MessageType int type) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        setToastMessageType(toast, type);
        toast.show();
    }

    /**
     * 显示toast，长时长，可选类型
     *
     * @param context 上下文
     * @param message 显示信息
     * @param type    显示类型
     */
    @SuppressLint("ShowToast")
    public static void showToastLongTime(Context context, @NonNull String message, @MessageType int type) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        setToastMessageType(toast, type);
        toast.show();
    }

    /**
     * 自定义toast布局
     *
     * @param toast    toast
     * @param context  上下文
     * @param layoutId 布局id
     * @param index    view的位置
     */
    public static void toastAddView(Toast toast, Context context, @LayoutRes int layoutId, int index) {
        View view = toast.getView();
        LinearLayout linearLayout = (LinearLayout) view;
        View addView = LayoutInflater.from(context).inflate(layoutId, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.gravity = Gravity.CENTER_VERTICAL;
        linearLayout.addView(addView, index, layoutParams);
    }

    /**
     * 设置预设消息类型
     *
     * @param toast toast
     * @param type  INFO,CONFIRM,WARRING,ERROR
     */
    private static void setToastMessageType(Toast toast, int type) {

        switch (type) {
            case INFO:
                setToastBackgroundColor(toast, BLUE);
                break;
            case CONFIRM:
                setToastBackgroundColor(toast, GREEN);
                break;
            case WARNING:
                setToastBackgroundColor(toast, ORANGE);
                break;
            case ERROR:
                setToastColor(toast, Color.YELLOW, RED);
                break;
        }
    }


    /**
     * 设置toast的文本颜色和背景颜色
     *
     * @param toast           toast
     * @param messageColor    文本颜色
     * @param backgroundColor 背景颜色
     */
    private static void setToastColor(Toast toast, @ColorInt int messageColor, @ColorInt int backgroundColor) {
        View view = toast.getView();
        if (view != null) {
            TextView message = view.findViewById(android.R.id.message);
            message.setBackgroundColor(backgroundColor);
            message.setTextColor(messageColor);
        }
    }

    /**
     * 设置toast的背景颜色，文本默认为WHITE
     *
     * @param toast           toast
     * @param backgroundColor 背景颜色
     */
    private static void setToastBackgroundColor(Toast toast, @ColorInt int backgroundColor) {
        View view = toast.getView();
        if (view != null) {
            TextView message = view.findViewById(android.R.id.message);
            message.setBackgroundColor(backgroundColor);
            message.setTextColor(WHITE);
        }
    }
}

