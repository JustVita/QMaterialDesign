package com.meijian.qp.materialdesign.datas;

import java.io.Serializable;

/**
 * 类名：com.meijian.qp.materialdesign.datas
 * 时间：2018/7/13 14:55
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class TabViewPagerMessage implements Serializable {

    private int imageId;
    private String title;

    public TabViewPagerMessage(String title, int imageId) {
        this.imageId = imageId;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
