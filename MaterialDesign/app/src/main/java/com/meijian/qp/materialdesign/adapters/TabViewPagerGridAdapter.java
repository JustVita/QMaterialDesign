package com.meijian.qp.materialdesign.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meijian.qp.materialdesign.R;
import com.meijian.qp.materialdesign.datas.TabViewPagerMessage;

import java.util.ArrayList;

/**
 * 类名：com.meijian.qp.materialdesign.adapters
 * 时间：2018/7/5 17:07
 * 描述：
 * 修改人：
 * 修改时间：
 * 修改备注：
 *
 * @author QP
 */
public class TabViewPagerGridAdapter extends RecyclerView.Adapter<TabViewPagerGridAdapter.MyGridViewHolder> {

    private ArrayList<TabViewPagerMessage> mData; // 名字和图片

    public TabViewPagerGridAdapter(ArrayList<TabViewPagerMessage> data) {
        mData = data;
    }

    @NonNull
    @Override
    public MyGridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyGridViewHolder holder = new MyGridViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tabvp_grid_item, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyGridViewHolder holder, final int position) {
        final int pos = holder.getAdapterPosition();
        holder.textView.setText(mData.get(pos).getTitle());
        holder.imageView.setImageResource(mData.get(pos).getImageId());

        if (mOnGridItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnGridItemClickListener.onItemClick(holder.itemView, pos);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mOnGridItemClickListener.onItemLongClick(holder.itemView, pos);
                    //拦截点击事件
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyGridViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        private TextView textView;

        public MyGridViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.grid_image);
            textView = itemView.findViewById(R.id.grid_text);
        }
    }

    private OnGridItemClickListener mOnGridItemClickListener;

    public void setOnGridItemClickListener(OnGridItemClickListener onGridItemClickListener) {
        this.mOnGridItemClickListener = onGridItemClickListener;
    }

    public interface OnGridItemClickListener {

        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }
}
