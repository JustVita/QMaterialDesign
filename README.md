# QMaterialDesign

   **Android Material Design Example**

## MainActivity
*** 
- toolbar + RecyclerView + CardView

![Main](http://t.bebove.com/data/members/main20180716153406.png)

## TabViewPagerActivity
***

- CoordinatorLayout + AppBarLayout + CollapsingToolbarLayout + TabLayout + ViewPager

![grid](http://t.bebove.com/data/members/tabViewPager20180716153218.png)

![](http://t.bebove.com/data/members/tabViewPager_content20180716153231.png)

## RecyclerViewWithFloatBtnActivity
***

- CoordinatorLayout + AppBarLayout + CollapsingToolbarLayout + RecyclerView + FloatingActionButton

![RvWithFb](http://t.bebove.com/data/members/RvWithFb20180716153201.png)